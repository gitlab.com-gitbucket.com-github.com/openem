RSpec.describe Auth do
  let(:provider) { nil }

  let(:auth_header) do
    {
      "provider" => provider,
      "uid" => "123",
      "info" => {
        "email" => "test@gitlab.com"
      }
    }
  end

  let(:payload) do
    {
      "uid" => auth_header["uid"],
      "email" => auth_header["info"]["email"]
    }
  end

  describe ".create_jwt_from_oauth_response" do
    subject { described_class.create_jwt_from_oauth_response(auth_header) }

    context "gitlab provider" do
      let(:provider) { "gitlab" }

      it "returns a string" do
        expect(subject).to be_a(String)
      end

      it "creates a JWT" do
        jwt = JWT.encode(
          payload,
          Auth.send(:private_key),
          Auth::ALGORITHM
        )

        expect(subject).to eq(jwt)
      end

      it "is decodeable" do
        data = Auth.decode(subject)

        expect(data).to eq([
          {
            "email" => "test@gitlab.com",
            "uid" => "123"
          },
          {
            "alg" => "ED25519"
          }
        ])
      end
    end

    context "unknown provider" do
      let(:provider) { "wibble" }

      it "raises an error" do
        expect { subject }.to raise_error(Auth::UnknownProviderError)
      end
    end
  end


  describe ".encode" do
    subject { described_class.encode(payload) }

    it "returns a JWT string" do
      expect(subject).to be_a(String)
    end
  end

  describe ".decode" do
    subject { described_class.decode(token) }

    let(:token) do
      Auth.encode(payload)
    end

    it "returns an array" do
      expect(subject).to be_a(Array)
    end

    it "returns the same payload" do
      expect(subject.first).to eq(payload)
    end
  end
end
