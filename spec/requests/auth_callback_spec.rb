RSpec.describe OpenEM::API do
  include Rack::Test::Methods

  def app
    subject
  end

  before do
    OmniAuth.config.test_mode = true

    OmniAuth.config.mock_auth[:gitlab] = OmniAuth::AuthHash.new({
      provider: "gitlab",
      uid: "123",
      info: {
        email: "test@gitlab.com"
      }
    })
  end

  describe "/auth/:provider/callback" do
    context "gitlab provider" do
      it "returns a 200 status" do
        get "/auth/gitlab/callback", nil, { "omniauth.auth" => OmniAuth.config.mock_auth[:gitlab] }

        expect(last_response.status).to eq(200)
      end
    end
  end
end
