RSpec.describe OpenEM::API do
  include Rack::Test::Methods

  def app
    subject
  end

  describe "/health" do
    it "returns a 200 status" do
      get "/health"

      expect(last_response.status).to eq(200)
    end
  end
end
