class Auth
  ALGORITHM = "ED25519".freeze

  UnknownProviderError = Class.new(StandardError)

  class << self
    def create_jwt_from_oauth_response(auth_header)
      case auth_header["provider"]
      when "gitlab"
        create_jwt_from_gitlab(auth_header)
      else
        raise UnknownProviderError
      end
    end

    def encode(payload)
      JWT.encode(payload, private_key, ALGORITHM)
    end

    def decode(token)
      JWT.decode(token, public_key, true, { algorithm: ALGORITHM })
    end

    private

    def create_jwt_from_gitlab(auth_header)
      payload = {
        uid: auth_header["uid"],
        email: auth_header["info"]["email"]
      }

      encode(payload)
    end

    def private_key
      @private_key ||= RbNaCl::Signatures::Ed25519::SigningKey.new(ENV["JWT_SECRET"])
    end

    def public_key
      @public_key ||= private_key.verify_key
    end
  end
end
