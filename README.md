# OpenEM
## An open-source intermediary API for endpoint management platforms

This is a small API which sits between end-users and the endpoint management
platform (e.g. Jamf), which allows those users to retrieve the information known
about them on the platform.
