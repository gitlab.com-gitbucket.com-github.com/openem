require "./environment"

#TODO: Do we need to use a cookie here?
#FIXME: Add a secret to this if so
use Rack::Session::Cookie

use OmniAuth::Builder do
  provider :gitlab, ENV["OAUTH_GITLAB_KEY"], ENV["OAUTH_GITLAB_SECRET"], {
    client_options: {
      site: "https://dev.gitlab.org/api/v4"
    },
    scope: "read_user openid"
  }
end

run OpenEM::API
