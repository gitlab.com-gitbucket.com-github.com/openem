require "./jamf/api"
require "./lib/auth"

module OpenEM
  class API < Grape::API
    format :json

    desc "Health polling endpoint"
    get :health do
      status :ok
    end

    desc "OAuth authorisation callback endpoint"
    get "/auth/:provider/callback" do
      token = Auth.create_jwt_from_oauth_response(request.env["omniauth.auth"])

      { jwt: token }
    end

    mount Jamf::API => "/jamf"
  end
end
